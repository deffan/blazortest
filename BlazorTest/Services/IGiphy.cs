﻿using BlazorTest.DTO;
using System.Threading.Tasks;

namespace BlazorTest.Services
{
    public interface IGiphy
    {
        Task<GiphySearchResult> Search(string searchString, int offset);
    }
}
