﻿using BlazorTest.DTO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace BlazorTest.Services
{
    public class Giphy : IGiphy
    {
        private readonly HttpClient m_Httpclient;
        private static string m_ApiKey;
        public const int PageSize = 9;
        private const string Rating = "g";
        private const string Language = "en";
        private const string Version = "v1";

        public Giphy(HttpClient httpclient)
        {
            m_Httpclient = httpclient;

            if (string.IsNullOrEmpty(m_ApiKey))
            {
                m_ApiKey = "n1SNTTlaogz1egPt7plVAFEJbIFVnnh9";
            }
        }

        public async Task<GiphySearchResult> Search(string searchString, int offset)
        {
            return await m_Httpclient.GetFromJsonAsync<GiphySearchResult>($"/{Version}/gifs/search?api_key={m_ApiKey}&q={searchString}&limit={PageSize}&offset={offset}&rating={Rating}&lang={Language}");
        }
    }
}
