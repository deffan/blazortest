﻿using BlazorTest.Components;
using BlazorTest.DTO;
using BlazorTest.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System.Threading.Tasks;

namespace BlazorTest.Pages
{
    public partial class Gifs
    {
        [Inject]
        IGiphy m_GiphyService { get; set; }

        private GiphySearchResult m_SearchResult;
        private int m_CurrentPage;
        private string m_CurrentSearchString;
        private string m_OldSearchString;

        protected override Task OnInitializedAsync()
        {
            m_SearchResult = new GiphySearchResult();
            m_CurrentPage = 0;
            m_CurrentSearchString = "";
            m_OldSearchString = "";
            return Task.CompletedTask;
        }

        private int GetTotalPages()
        {
            return m_SearchResult.pagination.total_count / Giphy.PageSize;
        }

        public async Task OnKeyDown(KeyboardEventArgs e)
        {
            if (e.Code == "Enter" || e.Code == "NumpadEnter")
            {
                await Search();
            }
        }

        private async Task NextPage(SimpleButton button)
        {
            // Validate that we can go to next page
            if ((m_CurrentPage + 1) * Giphy.PageSize > m_SearchResult.pagination.total_count)
            {
                return;
            }

            button.SetEnabled(false);
            m_CurrentPage++;
            await Search();
            button.SetEnabled(true);
        }

        private async Task PreviousPage(SimpleButton button)
        {
            // Validate that we can go to previous page
            if (m_SearchResult.pagination.total_count == 0 || m_CurrentPage - 1 < 0)
            {
                return;
            }

            button.SetEnabled(false);
            m_CurrentPage--;
            await Search();
            button.SetEnabled(true);
        }

        private async Task Search()
        {
            // Vad händer om vi söker på en felaktig URL? det hanteras inte....

            // Ignore empty searches
            if (string.IsNullOrEmpty(m_CurrentSearchString))
            {
                return;
            }

            // New search?
            if(!m_OldSearchString.Equals(m_CurrentSearchString))
            {
                m_OldSearchString = m_CurrentSearchString;
                m_CurrentPage = 0;
            }

            m_SearchResult = await m_GiphyService.Search(m_CurrentSearchString, m_CurrentPage * Giphy.PageSize);
        }
    }
}
